package day3;

import java.util.ArrayList;

public class Main {
    private static final String INPUT = """
            pqgZZSZgcZJqpzBbqTbbLjBDBLhB
            wHptFFsHttHFLMDQDFTbbj
            fVfvsstwPHwNwfNGfHWRSnlpClcJzCWCzddSrddg
            bdgHbZJHgMHgJgJctDtVssVcpFtq
            rNNQqBSzTcBPTDsP
            GWNNrhGnNnWNzRfnRQRbhCdqHMbdmbZddbmCmd
            BSBDzrSwrqccDDwbfcBjsRwggClslTRWGWGMFlsF
            dnhVhLJtzNZdgCRlsTGWCRJG
            ZHZdNzptLNtPhPdnprPbbDBrSqrSQPjbqD
            rlSwlrGvwTTSwSggCJGQJdhVgJGQ
            jcrHrMWfNHNzQgVH
            WbfjmBMrBrrmLtqMbwwsPTvPpwvBPFPsws
            NRNcHzbzbMRcNPjPrrlBPlbtBl
            CZwVCCpWggqprwtlHlssHtPr
            WpmLghCVCqCnmVTLnccRQvvQQHDhNQzzzc
            NvGTmNGDJsrCmCWPHpCP
            nqfVfnFQnZQfFqzMZBPtppcBPPCBptcrbF
            fhRSSVfqMZZhMnQfjVzShNNlLvldsNDdvvljGpllDT
            JGRNWRGJbGmCGRbLmGpqShhcQpQgCcncScSQ
            FFdtjvvpvVFnQhhnQhgf
            ltvjjtjHlzBtWRmNPLZRHLpH
            FFCJFsvgLsjLgWzJFWJgGwBDbwnbwlDddqffnjnlnd
            pTpTMQpMZHQhZQpHPZMmTMlwhDNNddbnDqdDwwlbVNVd
            SHpmrHPZHQpmvFrqsFvgCsRq
            TtWpWhQlVZrVptJhtrtdbLPDPbjFbCCWCvFFbLSN
            zMGlnlsgSSvjjGSF
            msznlgcwMnRwznmBqTZQJrddhfQJtBJtZQ
            mwTwLftmqqSHWfCfLHjWftBthNNNVrlcFRVNrNrRTrMgrRNg
            pvJPJQPGPPzbpVpVlMBVchFl
            bzQPQbQQdsDZPDGJnBGnPGnjHDttqqqqmCjftLCmWmfftC
            ZNpfdHcccTfdwfMFNjBttMgMbBnvlvjBmB
            QVPsGzhbszRLRrgvtgjWgljlRtgt
            VJrVLsSrzLzGPChVGzbrrfpTSHFfDDHSdpZFfHpDTZ
            NPpvDbmbsmdbNvQvDdWQpmWSnnQCLBnCcQSCnnLlwCBlZz
            jggrtGTFhtGfjhDVjrjgMftFwnZcwwBCnzzVwBBwSZcVwLSw
            JfHftHhgftgFJWPdPDWRPDvPJv
            ZSLLZJGglDSVNDGGGgGgngGmHrfLzmHvvjfjwLhHvLfHHr
            QqFWszFMTQFdFPMqBmWBHvfhCwCjhHff
            pTsdppTMPtqqdbnlNVzJVbSSnbZR
            gBqDccrrJDwmpTWHHTdWMPWWZFHF
            RNfnfSwRjlLSWjQMHWvQZtvH
            GRLbnzNnzVRLCqhwzwBmJrmc
            CcGnZGnGlRncsspmFmmcmGRJJzCDTzjLBSDfqjwDDzDLDB
            hHrNdPWhrbPdhPgVWvvrgWdfwjfNzSqfqjLqzfBTzJzJTz
            HHQhhvMWbbdRswmlsmwsQc
            rrwhpZPrccRpQdcFDJNqhtqtqMLhqfMh
            lTtTllgbzTlJsmDMvbLbsf
            VVWBTgzlzgGnngrtQRQRtCtwZRQB
            HGnGvVdLhlFcmvPWmT
            jwBLqZgjrtjqmFsQTscPQs
            ZBNZtwLwztLpMrfZBLMdbdnfSRVVfnGbnfDVGh
            LmBBBzQrBgBhmmggmtdVdhJNMHNdhsHNDd
            ScSZbRplCcMnSpvCfCCZcpPwtJPNtHPHNVVNtPddwlNH
            CpvpZGfnffSpnvRSSbcfScQBWrMWmjrmFFBzTGQWjMmL
            sljSjSgsjcCLllsjVgSjCtspQwvNNhdFwFQvwpbtmhwhpN
            hDqqWzGRHHfRrJnrWrfWfHBdFmBFmBbdBGBpwmFdFpww
            TDnDHZrWWHhTjPClClSP
            wGNQGQDGjqqmwHHs
            MWvvrzgfsdWsvMrSdqJqcpgHgnqLFLnjpH
            fvsPMsPdrTZhChNDhbhPVN
            sbMgDDtttVvpMtcJsgcGGBBfGLBSLclQTGPS
            CWHWzhhRRHjqRmSGPfBSzJfSBnBB
            HjjRHWFWhRRwHNmCCRHhhFdNDNrdptMstJvsbMDtVptd
            RnSwRsLsnSswjDDDBJPrJv
            cpzCzlczHTJVQhvBQlDVDj
            WmGzqTmHSsffqqnJ
            vQSPHMwpmpQMLGfTPVLRPRVP
            hsWhnncsJqbGjGfcfBMMRR
            sqdNWqqghbsJslgsJqgWllMWDppSvNCHQHDSSHrHrHCSvvCQ
            ZWWnWMmmndQZmffcdZcmssQqrsptVwwTtQHTCTHH
            SvvrPzvvFDzGzTszpGGwHT
            vrRLjjrPhLjrjPDSfdcMZnmdcmJcfcRf
            HpqWhDJjzmcTSbmMBVBb
            nCzfLtFnZZrcbcVVfTBfsc
            FtRFzgrRtnRzrFwzDjljpjgHNJDlNlhN
            gtNRRSSrRmjshHmm
            PQDMwPwMppcQQcvCFlhLhGmjflctlnHGjf
            QwDMFFdtwFDQJZZZNqSqJSqBgBqTNJNg
            cddzbbzQflTDcDfRbcfbJVsplVsChNghHNsSsVpn
            FBWFWjFFCjWPBSPPJsVpppPSVH
            CrCwvjWvmqmvrBvFwZRGQQDDcGTcfbddZdRc
            ZBQqdGLFmmzDmTZz
            PvrVMvGgWmwSmllglS
            NrfGnvWWPhfpspsGvLJBsFBbqJCbdQcLBq
            DrwTrlfGThhQTpDdWSWgdgwLLgBSZH
            bqbPRVRmzJCLWSgCzCLH
            jRtbNVtNjNqRqsJtbjbMDQHQGlchfQpfsTcHpGHr
            tMnRcnpDcZtpQDSCCsGGHvcGPGqGsr
            mzJmjWJNlbfmbhzVCCPmZrCZqPGCgZ
            JWhzjJBdWnMLZtSBTw
            tvdLttzvtHLztnQpssdTPbMqbqMTdTss
            jhSRGNjjSjhSDCNhRgRgclNjmfZTPnbTMqJJfqqsbMflTfMs
            WnCjcjDRCChSNSCNDjNhGVDtQvVLHzFrpFwFrHFQQwQpzp
            fLbLLLLQhVPhBVmDwmCfwsdwwDps
            GNtctFTSrrJqGGpHFcTJFTwsslwmlmWsdsqRRCmqwdWs
            ppctFTTSgHcSrGrrTGFcrrnhhQbPLbQZgzLvQQVVvZvLhv
            mBBWnnBbBCtssmRThRDllR
            wfwFQcpHFpddFrwpGcHSHdcjQZZlqqDTTjZqssRhWllssj
            dWfrcSGFpgrSzFgMbCPNPLtCtVMV
            llLlGLJJMjJMGVSvVMSLRRHvjCZtgZccttnmbCtdCcmCCztn
            sBQNqPhsrrqrrwrsppsHswsZcmnNCzdZtbgntcNgcctnCt
            WWFBBsPwpWPwBBHpFFrWGRMRLlJfJVMJRJLWSJ
            vgMvQnPMntnSQPSgMvSMpNJfJDNNRpfZmGQmbDND
            HlbjHHBLjCHGZGpfJLpfwm
            qdHWqBbbbjrTzdqFqssvtPMSSFFg
            TGDfDHSgtTzPPbnCtnNtVn
            WQrWMFpMWMQbCVNPRWVWWv
            pdpMMrhrprQshlMFjZpdjZMgmlGJJGlGDBmgmHgmJCSBHG
            zWWBjZZjWPFFPPnBCVdsqmnCdSLn
            rJvpbvbpGgTGrNJGGpRRhsCqSsLhnsmTVnSLTLHh
            gNNNJDbpvGNfvNSDPPWQWWPZZWtjlQfc
            RRVbWWWvvZVWmsFSsDNbHsDSsg
            CrTwJQJpJpCCwvlJQTTPsfzDgfwNhszfszFhzFDh
            ttQJtvjpPvcqTllJTPtPRGMGRGLWdVZLVZjWdMjj
            NnPCTQWMMQNNNWwWnMzpHczzsZcCscddHdGs
            mqRgqqVlLgqmfVzcGpzzSHGZcgcz
            tjmZjZmhqftlJRJhlTMPTPQbrPBBWnhnnT
            hvTQqpvTqjvhpjnCqmCnSDSFDWFFLSSSWDnSVL
            tZwGgsfPcltgcZltRgNSDSSSSldmWMLWFVHd
            GrZtwRPbGwwPcGRsZGtRtgQJJhCjpzmTBTvJzJrjvzQp
            rwmwqDWwfDtztnFGBB
            LPdpdVcdPGvPVgZsPtlhTTtthHBhHF
            dRdCjvpCRpjvCMZgvLgRVJJSWMqmbwQJbMWGWQNbbQ
            CMCcMcDGzBGPmBmznTNbnGbrswNTwTvN
            SSHVWZphqWWJJzNsbnFwFVNjbz
            flLQqHzzgtQdcDRB
            mdzvFtllBgFttGnvfMwMVRRZCThSNZVhMd
            pDTrDHjWWJPqjDjDSMqNwSZRZhNSRNCZ
            jpcTpQPWLLpDTLcTrPjPDcjzzFLFzvgLzlzfvGFgfmgFzF
            fQVVPzBpFVVrtrsJ
            PldSLNSmWwMCcCMMcCNN
            mSPlldllmPdRnLRwmbnLwmwvTjBTghTHQjfgjpZHpfHHfZbZ
            pmfMcfprMqMrZZJcMZMGWTsFCVCTVPPsVTWCGPDP
            vrvvvLRbBNNBbvBbjBHbQhgDslPTWsPTlFDsFTFwTWlDVQ
            hgjznNBjHHgrhRHgrRLRnRfSSJmdqMfffzqJptdmmmdd
            nRnPlCRPWPMFqwPLwq
            tBGfbSbHtBVQgrbrqfTFFLvTNLLNGTGMLdws
            bgHVtBDtqnqqlJRD
            SdSJrHssFBSVsNtMMdRWnTRhRl
            vcvfDvgvcwvFRlbnwWRlMhtn
            DDDqcqFZQPgcgcfvDjLDfVrsSVrHBLJVpLpCSppGpS
            gJGTFLTdrpLdBcWBvnllvlMvMC
            RRqbbQhwNZZwQRPrSZWnvHSZWSvSZC
            fQDNRsrsQzfbDrbsqwdtpgJVjdJdpVfJFLFF
            DzWqFvqpqFSCSzGRGmwfntGjmR
            cbhZNJQBtgMHJbJcNcrmfhrRrswmfRwnVrhG
            bJNgbNdJBBPMHbcMNMWWvSFpDLFvCStqpLdv
            sLsHTsTbRLRwqssHwHjFrPDwJDppzFDJmmcrPJ
            BnZGBlMZnQSgSnvVSMmJzPDCzFcrLPPJmpDG
            BgBffVLhQLgvnBRRssfqdfHbHdNT
            HRPVmjqBqVjVRRPmcPmJjbDgLDDshbfRLlfbfLbhlL
            rtTzSMSMFpTzfgDzzgsLfLHZ
            rNpGpSSHwMTrrdHGNtTPmVjnGGjVjmBGmmBjJB
            DBqDQDQHSFlHsFnN
            MfLfwwLMWGLrWMMnpSlsnGJJlbFVjV
            gRhMZzhrFLWQvTPqTPcvvh
            NwwsHwtnFCtzcPdbvrQbBqclQq
            VmZLLTLfVpwMBrVVqqMM
            mgJDjTgWgLWDppJZJTWZmSRzCtRHhGGwHNzshGFFCSRt
            RGCCDRdFZdRCMzzGCDGCmGHMfqbNNNLQLfFqnnqnNQqVPnQn
            glgcrwrJjJccBwdSfnSnVqrqQVVnNq
            jvtBsjstgstjltBcWzTGGddHTWDTZCmDGm
            HJHGZZHnctSSDhZtmZ
            MjjQFSvQlRjSdRqdqvVSqCCPtpRpPPDfDmfPbbpphC
            SNsWqMNvFFqdqVMgwwBHrGHnHgcWTJ
            jBcbjSmSBbbCcPcMjmbzFPhDMDfrfGRhGQRMnGQfdrDh
            wHlqwlqpwZqcwVlqHtJVJLTdhndTDnhffftTGDTTDdTG
            JNllcwpZZlpZJjNzSzSCNjSmFN
            FhwRPzmPWmQQmwFPGGMGGRPnRHHVfDbvJlvDlHSvDTDfVHbD
            NpjcpCdqpZZvwvJVfDdDHT
            twZtqrBrBQBMBPGn
            fBFGjbLLFblmbWFmVfBvrvMdMdncnrdNbdQNTr
            shZHHRZhtsqJZhHhgZzgJzVJrrSSvrMdMQrNTvMNJQNrdn
            szHwgtHtwPzPLpVFpVPLlfLC
            mrsrtrWjljjjvwwgNnZfDHJDqTqrHL
            FccMPFQcpczpdMPhMqJngNfqfnFgDnnFfg
            BdMpdcDPcpjBmlBmVWts
            VvwTTlfVlblwwSsbfTdzVqjhzVjpjjqjqpzV
            rwCWFGmJrNCmMRHmwRFPmHQQhBLBzdLqBjhLBHZdQB
            rFwDrMNRJDJFPRmCvcTcbDsvstTgfTsg
            zhRzdRRChHCFGPDRvWRWvWvHpZpscrrmrZrJcmspJmJZFfpM
            wQqLtQLtnjbjVnVjbBgjbBnbMZMZJlVpZfJprsMprmGZZZml
            jQjjNBLLwjtQBtwwdGGDCHhNzzWDzTPD
            DzzQnCMMznFnCdnFFlHtlmhVRtmVVmVhSF
            PWrPPRTfLJJtfbtBfV
            wsrggZsTwTTWGvDppQMRjjMCjMZp
            fTjzZVTlbffCMvjgMpSFWBNBWSFsvBsNNccF
            nJdwdPRQqGqbGJQbmmQQhRSBBBSsPPHWNSWFBtDNBsHH
            nnQhwwQGdLqqwnZbpfjMfzpzLbLj
            jgTgCwgjMgGLhvRrHrHwhvhV
            bqSsSsZFZBlFsBlTSppVvVvnVHHvHnhp
            qFlbPbFFsWFsBlFWbsbsmzTcMjLmtfcCmcWtgzgm
            rrHbfBLbfMcghcmrcCzg
            RDStDtvdZRQdJSQWWWdvFSgNvVcnghhmnnzhVPhczPch
            ZtJpJttSZStwtttFDQmLGTlqMLqGfwTTGLfTGG
            MrfLWwfBwgghvLmNvmHHHGGQHQSSscscVvTV
            dDjZjDPJtFRzjdTTsqVjTpqHsGrT
            JbFtlbPRJCzffBrgnlMWmg
            ZFsbbVLLdZppLFpcJjCCQJlGcQCMZq
            TwRtRBdBClCTGlcJ
            rwBvBzDvwNNDHLHzfHssdHhS
            gdhgftTNGTbpqJqjjgRJ
            lcBcMLFzMzLFMzFzPjRBQjQPQpSqhpbp
            mzzmZHZZnZwLhtGfddVsNCCnGG
            lblbPGSGrTLRwqZLvP
            FffCCFzFCWzzvmjRJnRTnZZNJCTqCR
            gdhztVjhHMsGvrGVVB
            ZJZjJGHZhDJRFJHjDZjhPNFgFmrnVmgVVzVBscnzSg
            bwlWtMwtbqdCvlQCplmsqgnVVScnVgmnmzNs
            WMWltTtvvCdwCCRPPfTHGcJDfGZL
            svqQJLvSSZrZZZCFCBDPDCMTDpPwMWDPCwRw
            GnlnGbdldjhzzhpPDTWjmtMwPmWW
            HzVbGnnbchblbnbzcQTZBZrQrFSHvLBBJv
            MmgMmVpcRDlvbvpHJF
            GSGTLTwhwwhzQqTqwjFlbdvdbrlrbrrDnDvHlQ
            zLNNNtwGFCMsWsCWNR
            tSTDDDftSqSsTDnTtCWNrbFsNJJvbzJbvJ
            dhRdVHdMGRgPJbjNPbzgvr
            VllQmQdhRHLhhHmLlGzSqSQDDcDBnnBnqDfSct
            zBzJWZBLZNNGLsbTvLbmbT
            QdtQwfdnPdPTbsRQGhRvbl
            pgtPgPjVDnpVnDtPTFFrJJTBCcpcrpCW
            GnWMfBfdCGMbjRNpnzvvNLRNVv
            FShJDJJscwwszNjvNjNNqZ
            tJmccwlcFlFcHlPcHFfdrbBGBGfjCGTfBCPf
            GhlcQsZNQZWhpcGhwlPmqnnqnjJjLRnqzJsJLJ
            VTMtTtDTbvbMTfvdJqngjmqzdjmJjCLm
            vHtbHTDBFvffBPGwLLZBQNNl
            bDphJrpbpnBbDrdBvJdDFBMtMlfgtsFSstfGPPgggPGP
            RZmNjTZQNVHQHNGSgMsfPlShSs
            VmchTLZQLjVLjmTVmQVhTmwVrWJqbDqddBrpnWbvnqrqcnJB
            jWWgThWtgSvSSWlJtlShllPcHVnJHPbMHPcPVPbVZrHM
            fGdfRsRdNwfRQhnpcZdVhVpbPh
            fGhwNBqNjqStFqtj
            TSTBrSDlQlTDrrQclrBSLffPvcfcdVjVMGGPLjLL
            qnbnbngFGhhhPfjjVffjff
            RRWbmgpnmqlrCwwSrwmG
            mZZTsdBZVZBZLVHdFmsNnCrCVQQbWvWjWNCnbg
            QSffDGwGGrPGWrgN
            hflwzltflDpMpDSllcMDhSShdsdZQdLZZdHTssZzmqLzFmLB
            LLRJRshLfsJfWnLBTlTBlFzNrnrBBl
            qmmVwmdHqmqGHZdHbbqSScdZQTjjpTFFVBBrlDrzDFBTjFjF
            wZZwmcbvHgqTmGccmvdCLhCPJsJCPWgMLPtJsJ
            TWbbbNbJJjJbqTjtJJjTQCtnGSBndMGCcSZSQwCB
            mcfRfrcmrDRrPsdQSGZQGnsSQMnB
            DDRLDRDFPpgmpcgPghpfgvRTjbhTVjHljJjzlVzVTlbHll
            rPlPrPllBGgJgdJfHgfjJt
            pppZVfFDWssMfFVVFMpsMMVmHCRLdcZCRtvLRdCtCJdHRttH
            mDMfDFDmnMMmsMFznDFpzswbNbPGwwSGBrGrhrTzThSl
            qDNFfCCNWLfWWhqhDGPMMZVwgpCpMbJwJCvV
            RdstRRvdtmtPVpppVbVtrp
            zzncSRdsTdQTczQBsLvvHNhDWGjDHNLDSG
            bNNpcfJcCtNpHFsJsGGjLGzmLjLmGzlFGW
            qwqZdnQnQwnhhzmnMWjmNlMzLr
            qhwwQSwStJbHNftS
            WlfWSwDftzRltBWVlRDlsmBJPcsZPmcJnmPmFhrn
            dLQbQbvGTddTvbjQCbLbhmCrZZPPsshPPPrJZrnF
            QgFjQHHbMvdRMVllSqfSlf
            MDPJBWWPggVlPVDMSljdZNNpwjwbHZpNbDdH
            mGmzcThGrtntHhthzGctRbNRNwRNzZwfdZpjpdRj
            ThtcvvtThFcnqFQSHgBSVJll
            hVqhFLBngHVFtJjtLCBJVSbbPNNbSmfLLTSNSrrLTb
            vsdZZpvQdczlMdMvzlcvvdQprbGGTfSbWmzPTgmmGTbmmfGP
            vgRZZMgwdgsQZdMBqVhjhJqBhJtRhq
            bgFQbMMbTbQhghddFTFGnmSmsNdzHvzSSzlcHsls
            fZDjVtfZLqwpqtCfCjCjlvqScrvzqHSzszzSnczr
            jpftjCfWCjCfCRZZlpCpjRWQBRTQQbgBBTMbghgbbPGJBJ
            VZZrbBVwbbbVVvgbntnggNRJqRRNNccMcNqJcJ
            jfDPfDdGGhDGfGFPCcZQqMpRNJhqTcMc
            LPfffPHGPDjPFGWSdHPFjWtlBlwvlwBlbtmLltsmvtZb
            TTfJDfrJTSrHMcVMJDTfMcMDBQBPwnPlznPszFVBFgzFgnsn
            CqtmWNNGBPzwbbwm
            htdCthhWGtWWGNZqcZpJjwwHHHMcHZDM
            VvjQjQCZLbbSbTPpSHtFzsHzppMfzz
            DJrJWBcDcWJWmmcgGRGRGWGDzHHwzdfRHpMztMpfdFdFdzdM
            DmBgGDqJNhGcccWmcZLjTPLVLTQhPtvvTZ
            qfhvwNDQqwDGdGZZGwPTTw
            STsJgsRtJMZPjlsmdpbs
            SCTTHTWHNVfHQqqq
            djCDgllgjJjDRRNgRlDdBgtpQHfhQTrLLrGBtzrQhpBH
            SVcsMGcPSbqSPmLTPHHQTHrftPTr
            VcWnsScqSScWcZbMMcSVGbNNlgDRlgCCNgwWvvRJdNdj
            mgPllfRgvNmPGQGGsmQNWlpFtnBPFShncTFShtFShnjS
            tLLzMJJwwbbdrrMLqLVJMzVZFnpTTFpnCSSpShCjBJhpThph
            HMdVwbbLMbDMDVlmDsgtNtNRfgsm
            hNsgsgzNZRghPhZBdssPQfzDmQSmmzQGJWzfCDJJ
            bblVHvvHHTljwFCfGrvmfmmJBmGQ
            THMMFVwqTPRdZptMBP
            QvcPGSvQLjmcQWSGWWGjLCNhhqpCdBCNCbJNdVWpCh
            rwtLlzZggLHnHlwHRDdVqBbCdqqVVhbqVnVh
            zRDzwRrwlRlRTgrDtllmQGLcPjGLccFmTcGSQc
            RWlgQlbcWBwzsJggTfhh
            GrnLjHLjmLjjGSLjSDmfJJpfThhfSWJPqJqhwz
            vLvDDnDNrCVjCmNDbFlBVZdVRQlRbWcb
            mTlwFngwmlLlvsmLHmHsLJhJFfcbdpbNcjCNCbpccb
            tZRzBRzBBRQzPqGRqrVQtjjfbCMcfMfCMMjVjfCJNd
            SDBBPtZZTdnnwSvg
            nddNNMMPNBnBNnBTQSShlSHghlDHBr
            VcccVmqJsJsjlTmzTDggmHHT
            VqLtFCqFJfVtVjsNgPNNMMWNwgtNvn
            """;

    public static void main(String[] args) {
        var totaal = 0;
        for (String s : INPUT.split("\n")) {
            var compartment1 = s.substring(0, s.length() / 2);
            var compartment2 = s.substring(s.length() / 2);
//            System.out.println("compartment1:" + compartment1);
//            System.out.println("compartment2:" + compartment2);
            var doubleItem = getDoubleChar(compartment1, compartment2);
//            System.out.println("item: " + doubleItem);
            var valueChar = formatCharToIng(doubleItem);
//            System.out.println("value: " + valueChar);
//            System.out.println("-----");
            totaal += valueChar;
        }
        System.out.println("Totaal challeng1: " + totaal);

        var count = 0;
        totaal = 0;
        var rucksacks = new ArrayList<String>();
        for (String s : INPUT.split("\n")) {
            count++;
            if (count == 3) {
                rucksacks.add(s);
                var trippleItem = getTrippleChar(rucksacks.get(0), rucksacks.get(1), rucksacks.get(2));
                var valueChar = formatCharToIng(trippleItem);
//                System.out.println("item: " + trippleItem);
//                System.out.println("value: " + valueChar);
//                System.out.println("-----");
                totaal += valueChar;
                // reset
                count = 0;
                rucksacks.remove(0);
                rucksacks.remove(0);
                rucksacks.remove(0);
            } else {
                rucksacks.add(s);
            }
        }
        System.out.println("Totaal challeng2: " + totaal);
    }

    private static char getDoubleChar(String input1, String input2) {
        for (char c : input1.toCharArray()) {
            if (input2.indexOf(c) != -1) {
                return c;
            }
        }
        throw new IllegalArgumentException("Mag niet");
    }

    private static char getTrippleChar(String input1, String input2, String input3) {
        for (char c : input1.toCharArray()) {
            if (input2.indexOf(c) != -1 && input3.indexOf(c) != -1) {
                return c;
            }
        }
        throw new IllegalArgumentException("Mag niet");
    }

    private static int formatCharToIng(char input) {
        var asciiChar = (int) input;
        if (asciiChar < 91) { // hoofdletters
            return asciiChar - 38;
        } else {// kleine letters
            return asciiChar - 96;
        }
    }
}