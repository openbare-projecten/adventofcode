package day5;

import java.util.*;

public class Main {
    private static final String INPUT = """
            [N]         [C]     [Z]           \s
            [Q] [G]     [V]     [S]         [V]
            [L] [C]     [M]     [T]     [W] [L]
            [S] [H]     [L]     [C] [D] [H] [S]
            [C] [V] [F] [D]     [D] [B] [Q] [F]
            [Z] [T] [Z] [T] [C] [J] [G] [S] [Q]
            [P] [P] [C] [W] [W] [F] [W] [J] [C]
            [T] [L] [D] [G] [P] [P] [V] [N] [R]
             1   2   3   4   5   6   7   8   9\s
                        
            move 6 from 2 to 1
            move 4 from 6 to 3
            move 1 from 6 to 5
            move 8 from 3 to 8
            move 13 from 8 to 2
            move 2 from 7 to 6
            move 10 from 1 to 6
            move 3 from 2 to 8
            move 5 from 4 to 2
            move 15 from 6 to 5
            move 1 from 1 to 4
            move 2 from 7 to 3
            move 2 from 4 to 2
            move 12 from 5 to 1
            move 4 from 8 to 9
            move 15 from 1 to 3
            move 10 from 9 to 7
            move 1 from 5 to 1
            move 1 from 4 to 8
            move 3 from 7 to 6
            move 8 from 2 to 6
            move 1 from 9 to 8
            move 5 from 2 to 3
            move 1 from 4 to 1
            move 16 from 3 to 1
            move 2 from 2 to 7
            move 13 from 1 to 6
            move 1 from 2 to 4
            move 2 from 2 to 9
            move 1 from 4 to 7
            move 2 from 8 to 2
            move 2 from 2 to 9
            move 1 from 6 to 8
            move 2 from 3 to 8
            move 2 from 1 to 9
            move 1 from 3 to 9
            move 1 from 3 to 2
            move 5 from 5 to 1
            move 2 from 9 to 3
            move 1 from 2 to 3
            move 2 from 1 to 3
            move 3 from 3 to 2
            move 1 from 5 to 7
            move 2 from 7 to 6
            move 2 from 8 to 3
            move 1 from 8 to 9
            move 6 from 3 to 4
            move 3 from 9 to 6
            move 8 from 6 to 4
            move 1 from 2 to 3
            move 1 from 2 to 6
            move 1 from 2 to 9
            move 1 from 3 to 9
            move 5 from 9 to 5
            move 7 from 7 to 4
            move 14 from 4 to 6
            move 1 from 5 to 3
            move 5 from 1 to 9
            move 4 from 5 to 4
            move 1 from 1 to 7
            move 1 from 3 to 8
            move 1 from 8 to 4
            move 4 from 9 to 7
            move 6 from 6 to 5
            move 10 from 4 to 6
            move 1 from 9 to 6
            move 1 from 4 to 3
            move 1 from 3 to 6
            move 1 from 4 to 2
            move 35 from 6 to 3
            move 1 from 2 to 3
            move 4 from 5 to 8
            move 2 from 5 to 4
            move 3 from 8 to 2
            move 2 from 4 to 8
            move 26 from 3 to 8
            move 3 from 2 to 9
            move 6 from 3 to 5
            move 3 from 5 to 7
            move 3 from 7 to 4
            move 3 from 4 to 5
            move 1 from 9 to 5
            move 6 from 5 to 1
            move 2 from 8 to 6
            move 11 from 8 to 5
            move 9 from 5 to 4
            move 1 from 9 to 7
            move 2 from 7 to 9
            move 3 from 1 to 4
            move 1 from 5 to 7
            move 8 from 6 to 1
            move 5 from 7 to 9
            move 7 from 9 to 2
            move 3 from 2 to 9
            move 3 from 7 to 1
            move 4 from 9 to 8
            move 2 from 5 to 6
            move 2 from 2 to 8
            move 2 from 6 to 9
            move 13 from 8 to 1
            move 1 from 2 to 8
            move 3 from 3 to 5
            move 1 from 9 to 8
            move 3 from 5 to 4
            move 1 from 9 to 3
            move 1 from 2 to 3
            move 4 from 8 to 2
            move 3 from 2 to 4
            move 19 from 1 to 2
            move 8 from 1 to 8
            move 1 from 4 to 3
            move 1 from 4 to 1
            move 7 from 2 to 1
            move 1 from 3 to 1
            move 2 from 3 to 1
            move 15 from 4 to 5
            move 1 from 1 to 7
            move 11 from 2 to 8
            move 2 from 2 to 9
            move 1 from 3 to 5
            move 2 from 9 to 4
            move 12 from 8 to 3
            move 16 from 5 to 1
            move 3 from 4 to 3
            move 1 from 7 to 5
            move 2 from 8 to 6
            move 1 from 5 to 4
            move 1 from 4 to 9
            move 18 from 1 to 9
            move 8 from 3 to 8
            move 9 from 8 to 2
            move 4 from 9 to 2
            move 8 from 1 to 2
            move 2 from 6 to 4
            move 17 from 2 to 1
            move 1 from 4 to 5
            move 3 from 2 to 6
            move 1 from 2 to 9
            move 2 from 6 to 1
            move 3 from 3 to 6
            move 1 from 4 to 6
            move 2 from 3 to 2
            move 16 from 9 to 5
            move 14 from 5 to 4
            move 3 from 5 to 8
            move 1 from 2 to 4
            move 4 from 8 to 6
            move 1 from 2 to 8
            move 1 from 3 to 9
            move 1 from 3 to 9
            move 2 from 9 to 1
            move 10 from 8 to 7
            move 7 from 6 to 9
            move 16 from 1 to 5
            move 7 from 4 to 3
            move 1 from 8 to 4
            move 5 from 4 to 2
            move 1 from 5 to 9
            move 5 from 9 to 1
            move 5 from 1 to 2
            move 2 from 9 to 7
            move 1 from 1 to 7
            move 1 from 6 to 8
            move 4 from 4 to 5
            move 1 from 6 to 9
            move 9 from 2 to 1
            move 11 from 5 to 6
            move 2 from 9 to 2
            move 4 from 3 to 4
            move 4 from 4 to 6
            move 1 from 3 to 4
            move 11 from 7 to 4
            move 9 from 4 to 7
            move 11 from 7 to 2
            move 2 from 3 to 5
            move 2 from 4 to 8
            move 7 from 5 to 2
            move 1 from 8 to 3
            move 1 from 5 to 1
            move 1 from 3 to 7
            move 6 from 2 to 9
            move 1 from 8 to 9
            move 6 from 9 to 2
            move 15 from 6 to 2
            move 1 from 7 to 2
            move 31 from 2 to 7
            move 22 from 7 to 3
            move 2 from 5 to 1
            move 3 from 7 to 4
            move 1 from 4 to 9
            move 3 from 4 to 3
            move 1 from 8 to 6
            move 1 from 9 to 6
            move 15 from 1 to 5
            move 1 from 9 to 5
            move 1 from 1 to 8
            move 2 from 6 to 8
            move 1 from 8 to 4
            move 1 from 4 to 6
            move 1 from 6 to 9
            move 10 from 3 to 1
            move 1 from 9 to 7
            move 2 from 7 to 8
            move 10 from 5 to 1
            move 12 from 1 to 4
            move 1 from 3 to 8
            move 11 from 4 to 8
            move 1 from 8 to 3
            move 5 from 5 to 8
            move 1 from 5 to 8
            move 6 from 8 to 6
            move 3 from 2 to 1
            move 1 from 6 to 2
            move 5 from 1 to 6
            move 3 from 1 to 4
            move 3 from 2 to 8
            move 1 from 2 to 9
            move 8 from 3 to 5
            move 2 from 1 to 3
            move 3 from 7 to 5
            move 2 from 3 to 5
            move 3 from 5 to 2
            move 1 from 7 to 9
            move 2 from 9 to 1
            move 1 from 6 to 9
            move 2 from 4 to 8
            move 5 from 6 to 5
            move 1 from 6 to 7
            move 1 from 9 to 8
            move 3 from 6 to 5
            move 7 from 8 to 9
            move 5 from 9 to 1
            move 2 from 4 to 8
            move 11 from 5 to 9
            move 3 from 2 to 3
            move 2 from 5 to 8
            move 4 from 3 to 7
            move 11 from 9 to 5
            move 3 from 7 to 5
            move 1 from 3 to 5
            move 8 from 1 to 4
            move 5 from 3 to 9
            move 15 from 5 to 4
            move 8 from 4 to 1
            move 12 from 8 to 1
            move 4 from 5 to 8
            move 12 from 4 to 5
            move 3 from 7 to 2
            move 11 from 5 to 7
            move 8 from 8 to 7
            move 7 from 9 to 8
            move 2 from 5 to 7
            move 4 from 7 to 8
            move 9 from 8 to 4
            move 11 from 4 to 5
            move 6 from 7 to 8
            move 9 from 8 to 7
            move 18 from 7 to 5
            move 1 from 8 to 1
            move 4 from 1 to 5
            move 1 from 7 to 2
            move 6 from 1 to 9
            move 1 from 2 to 4
            move 1 from 4 to 3
            move 3 from 1 to 7
            move 1 from 4 to 2
            move 3 from 2 to 5
            move 2 from 9 to 5
            move 1 from 2 to 6
            move 4 from 7 to 8
            move 1 from 6 to 2
            move 1 from 2 to 4
            move 4 from 8 to 5
            move 3 from 9 to 7
            move 1 from 9 to 5
            move 1 from 4 to 3
            move 2 from 3 to 8
            move 2 from 7 to 4
            move 28 from 5 to 8
            move 1 from 8 to 9
            move 1 from 9 to 3
            move 6 from 5 to 6
            move 5 from 5 to 2
            move 1 from 3 to 4
            move 1 from 7 to 4
            move 1 from 5 to 6
            move 16 from 8 to 3
            move 7 from 1 to 8
            move 4 from 4 to 9
            move 1 from 2 to 4
            move 3 from 2 to 3
            move 6 from 6 to 8
            move 10 from 3 to 8
            move 1 from 2 to 7
            move 1 from 6 to 7
            move 11 from 8 to 5
            move 2 from 7 to 8
            move 1 from 1 to 9
            move 5 from 9 to 5
            move 4 from 3 to 2
            move 1 from 4 to 2
            move 1 from 3 to 8
            move 3 from 8 to 2
            move 19 from 8 to 7
            move 6 from 7 to 6
            move 4 from 5 to 2
            move 9 from 7 to 5
            move 1 from 7 to 1
            move 5 from 6 to 9
            move 1 from 7 to 4
            move 1 from 6 to 7
            move 1 from 4 to 7
            move 1 from 1 to 2
            move 2 from 7 to 3
            move 6 from 5 to 9
            move 9 from 9 to 1
            move 17 from 5 to 4
            move 2 from 3 to 1
            move 13 from 4 to 7
            move 3 from 3 to 5
            move 7 from 1 to 4
            move 1 from 5 to 8
            move 2 from 5 to 2
            move 6 from 7 to 3
            move 1 from 5 to 7
            move 1 from 9 to 1
            move 2 from 3 to 2
            move 1 from 9 to 3
            move 9 from 7 to 3
            move 10 from 3 to 5
            move 8 from 4 to 2
            move 1 from 4 to 1
            move 13 from 2 to 4
            move 5 from 4 to 3
            move 1 from 5 to 9
            move 1 from 7 to 2
            move 6 from 4 to 2
            move 4 from 1 to 8
            move 3 from 4 to 6
            move 9 from 8 to 9
            move 17 from 2 to 3
            move 2 from 8 to 6
            move 1 from 4 to 3
            move 2 from 6 to 3
            move 2 from 1 to 3
            move 13 from 3 to 4
            move 8 from 9 to 8
            move 7 from 4 to 6
            move 3 from 5 to 6
            move 5 from 8 to 2
            move 9 from 6 to 1
            move 7 from 2 to 4
            move 2 from 6 to 9
            move 1 from 1 to 5
            move 18 from 3 to 8
            move 5 from 1 to 3
            move 1 from 6 to 1
            move 9 from 4 to 7
            move 11 from 8 to 7
            move 5 from 7 to 5
            move 2 from 4 to 5
            move 1 from 6 to 2
            move 13 from 7 to 8
            move 1 from 4 to 9
            move 1 from 9 to 6
            move 4 from 1 to 5
            move 1 from 7 to 6
            move 9 from 5 to 7
            move 8 from 5 to 6
            move 10 from 7 to 2
            move 1 from 5 to 7
            move 1 from 7 to 1
            move 17 from 8 to 2
            move 9 from 6 to 7
            move 6 from 7 to 1
            move 2 from 7 to 2
            move 1 from 4 to 2
            move 12 from 2 to 8
            move 7 from 1 to 2
            move 6 from 8 to 6
            move 3 from 8 to 2
            move 1 from 7 to 2
            move 2 from 3 to 4
            move 1 from 4 to 9
            move 2 from 3 to 5
            move 2 from 3 to 7
            move 1 from 4 to 6
            move 2 from 7 to 1
            move 7 from 2 to 7
            move 6 from 7 to 1
            move 1 from 5 to 2
            move 6 from 8 to 4
            move 4 from 9 to 7
            move 1 from 5 to 2
            move 3 from 8 to 1
            move 1 from 9 to 4
            move 1 from 7 to 8
            move 1 from 8 to 1
            move 4 from 7 to 8
            move 1 from 4 to 2
            move 3 from 6 to 9
            move 2 from 9 to 7
            move 1 from 9 to 3
            move 2 from 4 to 3
            move 2 from 8 to 3
            move 5 from 3 to 4
            move 4 from 6 to 2
            move 8 from 2 to 9
            move 1 from 6 to 5
            move 10 from 2 to 3
            move 2 from 8 to 3
            move 8 from 9 to 3
            move 9 from 2 to 5
            move 1 from 2 to 4
            move 1 from 2 to 3
            move 7 from 5 to 6
            move 1 from 5 to 7
            move 13 from 3 to 4
            move 2 from 7 to 8
            move 5 from 3 to 1
            move 1 from 5 to 3
            move 1 from 8 to 5
            move 1 from 2 to 8
            move 1 from 7 to 9
            move 1 from 4 to 2
            move 15 from 4 to 8
            move 6 from 4 to 7
            move 6 from 7 to 8
            move 1 from 6 to 5
            move 1 from 4 to 6
            move 1 from 9 to 6
            move 2 from 5 to 2
            move 6 from 6 to 4
            move 6 from 1 to 8
            move 6 from 4 to 9
            move 2 from 6 to 1
            move 1 from 2 to 9
            move 26 from 8 to 1
            move 4 from 3 to 7
            move 2 from 2 to 5
            move 16 from 1 to 4
            move 3 from 9 to 8
            move 3 from 8 to 7
            move 3 from 5 to 1
            move 2 from 9 to 2
            move 1 from 9 to 7
            move 1 from 9 to 1
            move 8 from 4 to 1
            move 4 from 4 to 9
            move 1 from 2 to 3
            move 1 from 3 to 7
            move 2 from 8 to 2
            move 3 from 4 to 2
            move 1 from 4 to 7
            move 9 from 7 to 5
            move 1 from 9 to 8
            move 2 from 9 to 8
            move 5 from 5 to 7
            move 1 from 9 to 5
            move 6 from 2 to 6
            move 1 from 8 to 2
            move 5 from 6 to 5
            move 1 from 7 to 4
            move 3 from 8 to 9
            move 3 from 9 to 7
            move 1 from 6 to 4
            move 2 from 4 to 1
            move 2 from 5 to 8
            move 1 from 2 to 9
            move 2 from 8 to 9
            move 3 from 9 to 3
            move 8 from 7 to 3
            move 4 from 5 to 8
            move 1 from 3 to 9
            move 3 from 5 to 8
            move 1 from 5 to 3
            move 6 from 8 to 6
            move 3 from 3 to 9
            move 5 from 3 to 2
            move 5 from 6 to 4
            move 14 from 1 to 5
            move 8 from 5 to 6
            move 2 from 3 to 2
            move 4 from 9 to 1
            move 1 from 8 to 7
            move 7 from 2 to 3
            move 6 from 3 to 7
            move 3 from 5 to 3
            move 1 from 3 to 9
            move 12 from 1 to 5
            move 1 from 9 to 7
            move 2 from 3 to 1
            move 1 from 7 to 8
            move 1 from 8 to 7
            move 2 from 3 to 6
            move 2 from 1 to 9
            move 2 from 5 to 6
            move 2 from 9 to 7
            move 9 from 7 to 3
            move 7 from 1 to 5
            move 5 from 5 to 2
            move 8 from 6 to 8
            move 5 from 8 to 9
            """;
    private static final HashMap<String, Deque<String>> STACKS = new HashMap();
    private static final Integer COLUMS = 9;
    private static final Integer ROWS = 8;

    public static void main(String[] args) {
        System.out.println("output1: " + output1());
        System.out.println("output2: " + output2());
    }

    private static String output2() {
        var output = new StringBuilder();
        int row = 1;
        for (int i = 1; i <= COLUMS; i++) {
            STACKS.put(String.valueOf(i), new ArrayDeque<>());
        }
        for (String s : INPUT.split("\n")) {
            if (row <= ROWS) {
                var tempInt = 0;
                for (int i = 0; i < COLUMS; i++) {
                    var crate = "";
                    if (i == COLUMS - 1) {
                        crate = s.substring(tempInt);
                    } else {
                        crate = s.substring(tempInt, tempInt + 4);
                    }
                    if (!crate.isBlank()) {
                        STACKS.get(String.valueOf(i + 1)).addFirst(crate.substring(1, 2));
                    }
                    tempInt += 4;
                }
                row++;
            } else {
                if (s.startsWith("move")) {
                    var temp = s.split(" ");
                    var amount = Integer.parseInt(temp[1]);
                    var from = STACKS.get(temp[3]);
                    var to = STACKS.get(temp[5]);
                    var tempStack = new ArrayDeque<String>();
                    for (int i = 0; i < amount; i++) {
                        tempStack.addFirst(from.getLast());
                        from.removeLast();
                    }
                    to.addAll(tempStack);
//                    System.out.println(amount);
//                    System.out.println(from);
//                    System.out.println(to);
//                    System.out.println("-----");
//                    System.out.println(STACKS);
                }
            }
        }
        for (Integer i = 1; i <= COLUMS; i++) {
            output.append(STACKS.get(String.valueOf(i)).getLast());
        }
        return output.toString();
    }
    private static String output1() {
        var output = new StringBuilder();
        int row = 1;
        for (int i = 1; i <= COLUMS; i++) {
            STACKS.put(String.valueOf(i), new ArrayDeque<>());
        }
        for (String s : INPUT.split("\n")) {
            if (row <= ROWS) {
                var tempInt = 0;
                for (int i = 0; i < COLUMS; i++) {
                    var crate = "";
                    if (i == COLUMS - 1) {
                        crate = s.substring(tempInt);
                    } else {
                        crate = s.substring(tempInt, tempInt + 4);
                    }
                    if (!crate.isBlank()) {
                        STACKS.get(String.valueOf(i + 1)).addFirst(crate.substring(1, 2));
                    }
                    tempInt += 4;
                }
                row++;
            } else {
                if (s.startsWith("move")) {
                    var temp = s.split(" ");
                    var amount = Integer.parseInt(temp[1]);
                    var from = STACKS.get(temp[3]);
                    var to = STACKS.get(temp[5]);
                    for (int i = 0; i < amount; i++) {
                        to.addLast(from.getLast());
                        from.removeLast();
                    }
//                    System.out.println(amount);
//                    System.out.println(from);
//                    System.out.println(to);
//                    System.out.println("-----");
//                    System.out.println(STACKS);
                }
            }
        }
        for (Integer i = 1; i <= COLUMS; i++) {
            output.append(STACKS.get(String.valueOf(i)).getLast());
        }
        return output.toString();
    }
}
