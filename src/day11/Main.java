package day11;

import java.math.BigInteger;
import java.util.*;

public class Main {
    private static final String INPUT = """
            Monkey 0:
              Starting items: 98, 89, 52
              Operation: new = old * 2
              Test: divisible by 5
                If true: throw to monkey 6
                If false: throw to monkey 1
                        
            Monkey 1:
              Starting items: 57, 95, 80, 92, 57, 78
              Operation: new = old * 13
              Test: divisible by 2
                If true: throw to monkey 2
                If false: throw to monkey 6
                        
            Monkey 2:
              Starting items: 82, 74, 97, 75, 51, 92, 83
              Operation: new = old + 5
              Test: divisible by 19
                If true: throw to monkey 7
                If false: throw to monkey 5
                        
            Monkey 3:
              Starting items: 97, 88, 51, 68, 76
              Operation: new = old + 6
              Test: divisible by 7
                If true: throw to monkey 0
                If false: throw to monkey 4
                        
            Monkey 4:
              Starting items: 63
              Operation: new = old + 1
              Test: divisible by 17
                If true: throw to monkey 0
                If false: throw to monkey 1
                        
            Monkey 5:
              Starting items: 94, 91, 51, 63
              Operation: new = old + 4
              Test: divisible by 13
                If true: throw to monkey 4
                If false: throw to monkey 3
                        
            Monkey 6:
              Starting items: 61, 54, 94, 71, 74, 68, 98, 83
              Operation: new = old + 2
              Test: divisible by 3
                If true: throw to monkey 2
                If false: throw to monkey 7
                        
            Monkey 7:
              Starting items: 90, 56
              Operation: new = old * old
              Test: divisible by 11
                If true: throw to monkey 3
                If false: throw to monkey 5
            """;

    private static final List<Monkey> monkeys = new ArrayList<>();

    public static void main(String[] args) {
        readInput();
//        runSimulation(20, true);
        runSimulation(10000, false);

        for (Monkey monkey1 : monkeys) {
            System.out.println(monkey1);
        }


        Long challenge1 = 1L;
        for (Integer integer : monkeys.stream()
                .sorted(Comparator.comparingInt(Monkey::getInspectLevel).reversed())
                .map(Monkey::getInspectLevel)
                .limit(2)
                .toList()) {
            challenge1 *= integer;
        }

        System.out.println(challenge1);

    }

    private static void runSimulation(int rounds, boolean challenge1) {
        for (int i = 1; i <= rounds; i++) {
//            System.out.println("round " + i);
            for (Monkey monkey : monkeys) {
                if (!monkey.getItems().isEmpty()) {
                    for (Item item : monkey.getItems()) {
                        monkey.operation(item);
                        item.releave(challenge1);
                        monkeys.get(monkey.testItem(item)).addItemAsLast(monkey.getAndRemoveItem());
                    }
                }
            }
        }
    }

    private static void readInput() {
        Monkey monkey = null;
        var itemId = 0;

        for (String s : INPUT.split("\n")) {
            if (s.trim().startsWith("Starting items")) {
                monkey = new Monkey();
                var itemString = s.replace("Starting items: ", "");
                var itemStringList = itemString.split(",");
                for (String str : itemStringList) {
                    monkey.addItemAsLast(new Item(itemId, Long.parseLong(str.trim())));
                    itemId++;
                }
            } else if (s.trim().startsWith("Operation")) {
                var operationString = s.replace("Operation: new = old ", "").trim();
                if (operationString.contains("*")) {
                    monkey.setOperation("*");
                    monkey.setOperationValue(operationString.replace("*", "").trim());
                } else if (operationString.contains("+")) {
                    monkey.setOperation("+");
                    monkey.setOperationValue(operationString.replace("+", "").trim());
                }
            } else if (s.trim().startsWith("Test")) {
                monkey.setTestDivder(Integer.parseInt(s.replace("Test: divisible by", "").trim()));
            } else if (s.trim().startsWith("If true")) {
                monkey.setMonkeyTrue(Integer.parseInt(s.replace("If true: throw to monkey", "").trim()));
            } else if (s.trim().startsWith("If false")) {
                monkey.setMonkeyFalse(Integer.parseInt(s.replace("If false: throw to monkey", "").trim()));
                // laatste regel dus toevoegen aan de lijst
                monkeys.add(monkey);
            }

        }
    }
}

class Monkey {
    private final Deque<Item> items = new ArrayDeque<>();
    private String operation;
    private String operationValue;
    private int testDivder;
    private int monkeyTrue;
    private int monkeyFalse;
    private int inspectLevel = 0;

    public int testItem(Item item) {
        var worry = item.getWorryLevel();
        item.setWorryLevel(worry);
        if (worry % testDivder == 0) {
            return monkeyTrue;
        } else {
            return monkeyFalse;
        }
    }

    public void operation(Item item) {
        inspectLevel++;
        Long value;
        if (operationValue.equals("old")) {
            value = item.getWorryLevel();
        } else {
            value = Long.parseLong(operationValue);
        }
        if (operation.equals("+")) {
            item.setWorryLevel(item.getWorryLevel() + (value));
        } else if (operation.equals("*")) {
            item.setWorryLevel(item.getWorryLevel() * (value));
        }

    }

    public Deque<Item> getItems() {
        return items;
    }

    public Item getAndRemoveItem() {
        return items.removeFirst();
    }

    public void addItemAsLast(Item item) {
        items.addLast(item);
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public void setTestDivder(int testDivder) {
        this.testDivder = testDivder;
    }

    public void setMonkeyTrue(int monkeyTrue) {
        this.monkeyTrue = monkeyTrue;
    }

    public void setMonkeyFalse(int monkeyFalse) {
        this.monkeyFalse = monkeyFalse;
    }

    public void setOperationValue(String operationValue) {
        this.operationValue = operationValue;
    }

    public int getInspectLevel() {
        return inspectLevel;
    }

    @Override
    public String toString() {
        return "Monkey{" +
                "items=" + items +
                ", operation='" + operation + '\'' +
                ", operationValue='" + operationValue + '\'' +
                ", testDivder=" + testDivder +
                ", monkeyTrue=" + monkeyTrue +
                ", monkeyFalse=" + monkeyFalse +
                ", inspectLevel=" + inspectLevel +
                '}';
    }
}

class Item {
    private final int id;
    private Long worryLevel;

    public Item(int id, Long worryLevel) {
        this.id = id;
        this.worryLevel = worryLevel;
    }

    public Long getWorryLevel() {
        return worryLevel;
    }

    public void setWorryLevel(Long worryLevel) {
        this.worryLevel = worryLevel;
    }

    public void releave(boolean challenge1) {
        if (challenge1) {
            worryLevel = worryLevel / 3;
        } else {
            worryLevel = worryLevel % 9699690;
            // magic nummer met hulp van https://chasingdings.com/2022/12/11/advent-of-code-day-11-monkey-in-the-middle/
        }
    }

    @Override
    public String toString() {
        return "worryLevel=" + worryLevel;
    }
}