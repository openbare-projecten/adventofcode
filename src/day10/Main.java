package day10;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

public class Main {
    private static final String INPUT = """
            addx 1
            addx 4
            addx 1
            noop
            addx 4
            addx 3
            addx -2
            addx 5
            addx -1
            noop
            addx 3
            noop
            addx 7
            addx -1
            addx 1
            noop
            addx 6
            addx -1
            addx 5
            noop
            noop
            noop
            addx -37
            addx 7
            noop
            noop
            noop
            addx 5
            noop
            noop
            noop
            addx 9
            addx -8
            addx 2
            addx 5
            addx 2
            addx 5
            noop
            noop
            addx -2
            noop
            addx 3
            addx 2
            noop
            addx 3
            addx 2
            noop
            addx 3
            addx -36
            noop
            addx 26
            addx -21
            noop
            noop
            noop
            addx 3
            addx 5
            addx 2
            addx -4
            noop
            addx 9
            addx 5
            noop
            noop
            noop
            addx -6
            addx 7
            addx 2
            noop
            addx 3
            addx 2
            addx 5
            addx -39
            addx 34
            addx 5
            addx -35
            noop
            addx 26
            addx -21
            addx 5
            addx 2
            addx 2
            noop
            addx 3
            addx 12
            addx -7
            noop
            noop
            noop
            noop
            noop
            addx 5
            addx 2
            addx 3
            noop
            noop
            noop
            noop
            addx -37
            addx 21
            addx -14
            addx 16
            addx -11
            noop
            addx -2
            addx 3
            addx 2
            addx 5
            addx 2
            addx -15
            addx 6
            addx 12
            addx -2
            addx 9
            addx -6
            addx 7
            addx 2
            noop
            noop
            noop
            addx -33
            addx 1
            noop
            addx 2
            addx 13
            addx 15
            addx -21
            addx 21
            addx -15
            noop
            noop
            addx 4
            addx 1
            noop
            addx 4
            addx 8
            addx 6
            addx -11
            addx 5
            addx 2
            addx -35
            addx -1
            noop
            noop
            """;

    private static Integer x = 1;
    private static Integer cycle = 0;

    private static Integer totaal = 0;
    private static ArrayList<String> row = new ArrayList<>();

    public static void main(String[] args) {
        for (String s : INPUT.split("\n")) {
            if (s.equals("noop")) {
                addCycle();
            } else if (s.startsWith("addx")) {
                addCycle();
                addCycle();
                var aantal = Integer.parseInt(s.split(" ")[1]);
                x += aantal;
            }
        }
        System.out.println("Challenge 1: " + totaal);
    }

    private static int position = 0;

    private static void addCycle() {
//        System.out.println(getSprite());
        row.add(position, getSprite().substring(position, position + 1));
        position++;
        cycle++;
        switch (cycle) {
            case 40 -> encCycle(20); // voor challenge 1 dit veranderen naar 20
            case 80 -> encCycle(60); //60
            case 120 -> encCycle(120); //100
            case 160 -> encCycle(160); //140
            case 200 -> encCycle(200); //180
            case 240 -> encCycle(240); //220
        }
    }

    private static String getSprite(){
        var sprite = new StringBuilder();
//        System.out.println(x);
        if (x == -1){
            sprite.append("#.......................................");
        }  else if(x ==39){
            sprite.append(".......................................#");
        }
        else if(x ==0){
            sprite.append("##......................................");
        }else {
            for (int i = 1; i <= 38; i++) {
                if (i == x) {
                    sprite.append("#");
                    sprite.append("#");
                    sprite.append("#");
                } else {
                    sprite.append(".");
                }
            }
        }
        return sprite.toString();
    }

    private static void encCycle(int number){
        totaal += number * x;
        System.out.println(row);
        row.clear();
        position = 0;
    }
}